package com.training3.imdb_app;

import com.training3.imdb_app.Getter_Setter.ImdbModel;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

public interface Api_interface {

    @Multipart
    @POST("/popular?api_key=676eeceb42c83ae99f98f7683077a7de"
    )
    void getdata(@Part("") String string,
                           Callback<ImdbModel> response);
}
