package com.training3.imdb_app.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.training3.imdb_app.R;

public class Splash_screen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private TextView txt_imdb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        txt_imdb=findViewById(R.id.txt_imdb);

        Animation animation= AnimationUtils.loadAnimation(Splash_screen.this,R.anim.rotate_move_down);
        txt_imdb.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                Intent i = new Intent(Splash_screen.this, MainActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
