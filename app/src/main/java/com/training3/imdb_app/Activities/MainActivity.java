package com.training3.imdb_app.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.training3.imdb_app.Adapter.Recyclerview_Adapter;
import com.training3.imdb_app.Api_interface;
import com.training3.imdb_app.Constant;
import com.training3.imdb_app.Getter_Setter.ImdbModel;
import com.training3.imdb_app.Getter_Setter.Result;
import com.training3.imdb_app.R;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Recyclerview_Adapter recyclerview_adapter;
    List<Result> listinflate = new ArrayList<Result>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.recyclerview);

        getdata();
    }

    private void getdata() {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.CONSTANT).build();
        final Api_interface api_interface = restAdapter.create(Api_interface.class);

        api_interface.getdata("", new Callback<ImdbModel>() {
            @Override
            public void success(ImdbModel imdbModel, Response response) {


                listinflate = imdbModel.getResults();
                recyclerView.setVisibility(View.VISIBLE);
                recyclerview_adapter = new Recyclerview_Adapter(MainActivity.this, listinflate);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MainActivity.this,2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setAdapter(recyclerview_adapter);
            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(MainActivity.this, "Connection Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menus, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }
}
